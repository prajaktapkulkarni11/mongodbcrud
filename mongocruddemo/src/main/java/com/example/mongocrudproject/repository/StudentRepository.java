package com.example.mongocrudproject.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.mongocrudproject.model.Student;

public interface StudentRepository extends MongoRepository<Student, String>{

}
