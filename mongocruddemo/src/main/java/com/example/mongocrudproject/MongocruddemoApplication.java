package com.example.mongocrudproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongocruddemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongocruddemoApplication.class, args);
	}

}
