package com.example.mongocrudproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.mongocrudproject.model.Student;
import com.example.mongocrudproject.repository.StudentRepository;

@RestController
public class StudentController {
	
	@Autowired
	private StudentRepository studentRepository;
	
	@PostMapping("/saveStudent")
	public Student saveStudent(@RequestBody Student student) {
		studentRepository.save(student);
		return student;
	}

}
